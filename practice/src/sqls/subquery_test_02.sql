/*
 * 请告诉我所有订单（order）中的订单明细的最大数目、最小数目和平均数目。结果应当包含三列：
 *
 * +────────────────────+────────────────────+────────────────────+
 * | minOrderItemCount  | maxOrderItemCount  | avgOrderItemCount  |
 * +────────────────────+────────────────────+────────────────────+
 */

SELECT min((SELECT count(*)
            FROM orderdetails
            WHERE orderdetails.orderNumber = orders.orderNumber))        AS minOrderItemCount,
       max((SELECT count(*)
            FROM orderdetails
            WHERE orderdetails.orderNumber = orders.orderNumber))        AS maxOrderItemCount,
       round(avg((SELECT count(*)
                  FROM orderdetails
                  WHERE orderdetails.orderNumber = orders.orderNumber))) AS avgOrderItemCount
FROM orders;
